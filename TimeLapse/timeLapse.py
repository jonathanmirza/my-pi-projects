#!/usr/bin/env python

import os
import time

from datetime import datetime

fileSerial = 1

while True:
    
    d = datetime.now()
    if d.hour >= 1:
        
        fileSerialNumber = "%04d" % (fileSerial)
        hour = "%02d" % (d.hour)
        mins = "%02d" % (d.minute)
    
        imgWidth = 800
        imgHeight = 600
    
        print "Taking photo"
        os.system("sudo raspistill -w " + str(imgWidth) + " -h " + str(imgHeight) + " -o " + "/home/pi/img01.jpg")
        print "Uploading to DropBox"
        os.system("/home/pi/Dropbox-Uploader/dropbox_uploader.sh upload /home/pi/img01.jpg " + str(fileSerialNumber) + "_" + str(hour) + str(mins) + ".jpg")
    
        fileSerial = fileSerial + 1
    
        time.sleep(180)
    