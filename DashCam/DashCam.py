#!/usr/bin/env python

import os
import time
import timeit
import pifacedigitalio
import picamera

from datetime import datetime

pfd = pifacedigitalio.PiFaceDigital()
camera = picamera.PiCamera()

#while True:

def toggle_LED0(event):
    event.chip.leds[0].toggle()
    
def START_RECORD(event):    
    for x in range (1,6):
        
        fileSerial = x
        fileSerialNumber = "%04d" % (fileSerial)
            #d = datetime.now()
            #year = "%04d" % (d.year)
            #month = "%02d" % (d.month)
            #day = "%02d" % (d.day)
            #hour = "%02d" % (d.hour)
            #mins = "%02d" % (d.minute)
            #sec = "%02d" % (d.second)
        
        
            #tic = timeit.default_timer()
            #os.system("sudo raspivid -n -t 10000 -o " + "/home/pi/DashCamVids/" + str(year) + "_" + str(month) + "_" + str(day) + "_" + str(hour) + str(mins) + str(sec) + ".h264")
        pfd.leds[7].turn_on()
            #os.system("sudo raspivid -n -t 300000 -o " + "/home/pi/DashCamVids/" + str(fileSerialNumber) + ".h264")
        
            #toc = timeit.default_timer()
        camera.start_recording("/home/pi/DashCamVids/" + str(fileSerialNumber) + ".h264")
            #print toc-tic
        camera.wait_recording(300)
        camera.stop_recording()
        pfd.leds[7].turn_off()
        time.sleep(2)
    
def STOP_RECORD(event):
        camera.stop_recording()
        pfd.leds[7].turn_off()
    
listener = pifacedigitalio.InputEventListener(chip=pfd)
listener.register(0, pifacedigitalio.IODIR_ON, START_RECORD)
listener.register(0, pifacedigitalio.IODIR_FALLING_EDGE, STOP_RECORD)
listener.activate()
    

    
