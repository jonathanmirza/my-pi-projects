#!/usr/bin/env python

# PiPong - A remake of the classic Pong game using PyGame. Written by
# Liam Fraser - 28/07/2012 for the Linux User & Developer magazine.

# Modified by Jonny Mirza

import pygame # Provides what we need to make a game
import sys # Gives us the sys.exit function to close our program
import random # Can generate random positions for the pong ball
import math # Gives us trig functions
import numpy as N
import pygame.surfarray as surfarray

from pygame.locals import *
from pygame import *

# Our main game class
class PiPong:
    
    def __init__(self):
    
    	# Make the display size a member of the class
		self.displaySize = (720, 576)
		
		# Initialize pygame
		pygame.init()
		
		# Create a clock to manage time
		self.clock = pygame.time.Clock()
		
		# Set the window title
		display.set_caption("")
		
		# Create the window
		self.display = display.set_mode(self.displaySize, pygame.FULLSCREEN)

		# Create the background, passing through the display size
		self.background = Background(self.displaySize)
	
		self.ball = Ball(self.displaySize)
		self.sprites = sprite.Group(self.ball)
	
    def run(self):
		# Runs the game loop
		
		while True:
			# The code here runs when every frame is drawn
					
			# Handle Events
			self.handleEvents()
			
			# Draw the background
			self.background.draw(self.display)
						
			# Update and draw the sprites
			self.sprites.update()
			self.sprites.draw(self.display)
	
			# Update the full display surface to the screen
			display.update()
			
			# Limit the game to 30 frames per second
			self.clock.tick(30)
			
    def handleEvents(self):
		
		# Handle events, starting with the quit event
		for event in pygame.event.get():
			if event.type == QUIT:
				pygame.quit()
				sys.exit()
			
			if event.type == KEYDOWN:
				# Find which key was pressed and start moving appropriate bat
				if event.key == K_DOWN:
					self.ball.down()
				elif event.key == K_UP:
					self.ball.up()
				if event.key == K_p:
					self.ball.changeBallColour("bright")
				elif event.key == K_MINUS:
					self.ball.changeBallColour("dark")
				if event.key == K_c:
					self.ball.startBallRoation()
				if event.key == K_r:
					self.ball.stopBallRotation()
					self.ball.removeBackgroundNoise()
					self.background.removeBackgroundNoise()
					self.ball.reset()
				if event.key == K_LEFT:
					self.ball.left()
				elif event.key == K_RIGHT:
					self.ball.right()
				if event.key == K_n:
					self.background.addBackgroundNoise()
				if event.key == K_m:
					self.ball.addBackgroundNoise()
			
			if event.type == KEYUP:
				if event.key == K_p or event.key == K_MINUS:
					self.ball.stopBallColourChange()
				#if event.key == K_c:
				#	self.ball.stopBallRoation()
				
# The class for the background
class Background:
	
	def __init__(self, displaySize):
		
		# Set our image to a new surface, the size of the screen rectangle
		self.image = Surface(displaySize)
		print self.image
		
		# Fill the image with a black colour (specified as R,G,B)
		self.image.fill((0,0,0))
		
		self.addNoise = False
		
		# Get width proportionate to display size
		lineWidth = displaySize[0] / 80
		#Counter for noise arrays
		self.count = 1
		
	def addBackgroundNoise(self):
		self.addNoise = True
		lower = 100
		upper = 255
		
		rgbarray = surfarray.array3d(self.image)
		rgbarray[:,:,:] = 0
		
		rgbarray[:,:,1] = N.random.randint(lower,upper,size=(720,576))
		self.image = surfarray.make_surface(rgbarray)

		rgbarray[:,:,1] = N.random.randint(lower,upper,size=(720,576))
		self.image2 = surfarray.make_surface(rgbarray)

		rgbarray[:,:,1] = N.random.randint(lower,upper,size=(720,576))
		self.image3 = surfarray.make_surface(rgbarray)

	def removeBackgroundNoise(self):
		self.addNoise = False
		# Fill the image with a green colour (specified as R,G,B)
		self.image.fill((0, 0, 0))
				
	def draw(self, display):
		if self.addNoise != True:
			display.blit(self.image, (0,0))	
		if self.addNoise:
			# Fill the 3D array with random values 0-255
			if self.count == 1:
				display.blit(self.image, (0,0))	

			elif self.count == 2:
				display.blit(self.image2, (0,0))	

			elif self.count == 3:
				display.blit(self.image3, (0,0))	

			if self.count > 1:
				self.count = 1
			else:
				self.count = 1

# The class for the bats on either side
class Bat(sprite.Sprite):
	
	def __init__(self, displaySize, player):
			
		# Initialize the sprite base class
		super(Bat, self).__init__()
		
		# Make player a member variable
		self.player = player
		
		# Get a width and height values proportionate to the display size
		width = displaySize[0] / 80
		height = displaySize[1]
		
		# Create an image for the sprite using the width and height
		# we just worked out
		self.image = Surface((width, height))
		
		# Fill the image white
		self.image.fill((255, 255, 255))
		
		# Create the sprites rectangle from the image
		self.rect = self.image.get_rect()
		
		# Set the rectangle's location depending on the player
		if player == "player1":
			# Left side
			self.rect.centerx = displaySize[0] / 20
		elif player == "player2":
			# Right side
			self.rect.centerx = displaySize[0] - displaySize[0] / 20
		elif player == "player3":
			#Player 3 Bat
			self.image = Surface((displaySize[0], height / 80))
			self.image.fill((255, 255, 255))
			self.rect = self.image.get_rect()
			self.rect.centerx = displaySize[0] / 2
			self.rect.centery = displaySize[1] - 10
		elif player == "player4":
			#Player 4 Bat
			self.image = Surface((displaySize[0], height / 80))
			self.image.fill((255, 255, 255))
			self.rect = self.image.get_rect()
			self.rect.centerx = displaySize[0] / 2
			self.rect.centery = displaySize[1] - displaySize[1] + 10
		
		# Center the rectangle vertically
		#self.rect.centery = displaySize[1] / 2
		
		# Set a bunch of direction and moving variables
		self.moving = False
		self.direction = "none"
		self.speed = 13
		
	def startMove(self, direction):
		
		# Set the moving flag to true
		self.direction = direction
		self.moving = True
		
	def update(self):
		
		if self.moving:
			# Move the bat up or down if moving
			if self.direction == "up":
				self.rect.centery -= self.speed
			elif self.direction == "down":
				self.rect.centery += self.speed
				
	def stopMove(self):

		self.moving = False

# The class for the ball
class Ball(sprite.Sprite):
	
	def __init__(self, displaySize):

		# Set RGB values
		red = 255
		green = 255
		blue = 255	

		angle = 0
				
		self.red = red
		self.green = green
		self.blue = blue
		self.angle = angle
		
		# Initialize the sprite base class
		super(Ball, self).__init__()
		
		# Get the display size for working out collisions later
		self.displaySize = displaySize
		
		# Get a width and height values proportionate to the display size
		width = displaySize[0] / 30
		height = displaySize[1] / 30
		
		self.width = width
		self.height = height
				
		# Create an image for the sprite
		self.image = Surface((width, height))
		
		# Fill the image blue
		self.image.fill((self.red, self.green, self.blue))
		
		# Create the sprites rectangle from the image
		self.rect = self.image.get_rect()
		r = self.rect.centerx = self.displaySize[0] / 4
		self.r = r
			
		# Work out a speed
		self.speed = displaySize[0] / 600
	
		# Reset the ball
		self.reset()
		
		self.changeBcolour = False
		self.startBRotation = False
		self.addNoise = False
		self.intensity = "none"
		self.count = 1
		
	def rotateBall(self):
		if self.angle >= 360:
			self.angle = 0
		
		# Compute the parametric points of a circle
		self.rect.centerx = self.displaySize[0] / 2 + (self.r * math.cos(self.angle))
		self.rect.centery = self.displaySize[1] / 2 + (self.r * math.sin(self.angle))
		self.angle = self.angle + 0.02
						
	def startBallRoation(self):
		self.startBRotation = True
		
	def stopBallRotation(self):
		self.startBRotation = False 
		
	def changeBallColour(self, intensity):
		self.changeBcolour = True
		self.intensity = intensity
	
	def stopBallColourChange(self):
		self.changeBcolour = False

	def changeBallColourLight(self):
	
		if self.red == 255:
			self.red = self.red + 0
			self.green = self.green + 0
			self.blue = self.blue + 0
			self.image.fill((self.red, self.green, self.blue))
		elif self.red > 245:
			self.red = self.red + 1
			self.green = self.green + 1
			self.blue = self.blue + 1
		else:
			self.red = self.red + 5
			self.green = self.green + 5
			self.blue = self.blue + 5
			self.image.fill((self.red, self.green, self.blue))
			
	def changeBallColourDark(self):
		
		if self.red == 0:
			self.red = self.red - 0
			self.blue = self.blue - 0
			self.green = self.green - 0
		elif self.red < 10:
			self.red = self.red - 1
			self.green = self.green - 1
			self.blue = self.blue - 1
		else:
			self.red = self.red - 5
			self.green = self.green - 5
			self.blue = self.blue - 5
	
	def reset(self):
		
		# Start the ball directly in the centre of the screen
		self.rect.centerx = self.displaySize[0] / 2
		self.rect.centery = self.displaySize[1] / 2
		
		# Start the ball moving to the left or right (pick randomly)
		# Vector(x, y)
		if random.randrange(1, 3) == 1:
			# move to left
			self.vector = (-1, 1)
		else:			# move to right
			self.vector = (1, 1)
			
	def right(self):
		# Stop ball from moving in circle if doing so
		self.stopBallRotation()
		
		# Start the ball directly in the centre of the screen
		self.rect.centerx = self.displaySize[0] / 2
		self.rect.centery = self.displaySize[1] / 2
		
		self.vector = (1, 0)
		
	def left(self):
		# Stop ball from moving in circle if doing so
		self.stopBallRotation()
		
		# Start the ball directly in the centre of the screen
		self.rect.centerx = self.displaySize[0] / 2
		self.rect.centery = self.displaySize[1] / 2
		
		self.vector = (-1, 0)
		
	def up(self):
		# Stop ball from moving in circle if doing so
		self.stopBallRotation()
		
		# Start the ball directly in the centre of the screen
		self.rect.centerx = self.displaySize[0] / 2
		self.rect.centery = self.displaySize[1] / 2
		
		self.vector = (0, -1)
		
	def down(self):
		# Stop ball from moving in circle if doing so
		self.stopBallRotation()
		
		# Start the ball directly in the centre of the screen
		self.rect.centerx = self.displaySize[0] / 2
		self.rect.centery = self.displaySize[1] / 2
		
		self.vector = (0, 1)
		
	def addBackgroundNoise(self):
		self.addNoise = True
		
		lower = 150
		upper = 190
		
		# Fill the array with random values within a specified range
		# self.rgbarray = N.random.randint(lower,upper,size=(self.width,self.height))
		# self.rgbarray2 = N.random.randint(lower,upper,size=(self.width,self.height))
		# self.rgbarray3 = N.random.randint(lower,upper,size=(self.width,self.height))
		# print self.rgbarray
		
		self.rgbarray = surfarray.array3d(self.image)
		self.rgbarray2 = surfarray.array3d(self.image)
		self.rgbarray3 = surfarray.array3d(self.image)
		print "Ball self.image"
		print self.image
		print "Printing Ball RGB array"
		print self.rgbarray
        
		self.rgbarray[:,:,:] = 0
		self.rgbarray2[:,:,:] = 0
		self.rgbarray3[:,:,:] = 0
		
		
		self.rgbarray[:,:,1] = N.random.randint(lower,upper,size=(self.width,self.height))
		print "Array1"
		print self.rgbarray

		self.rgbarray2[:,:,1] = N.random.randint(lower,upper,size=(self.width,self.height))
		print "Array2"
		print self.rgbarray2

		self.rgbarray3[:,:,1] = N.random.randint(lower,upper,size=(self.width,self.height))
		print "Array3"
		print self.rgbarray3
		
	def removeBackgroundNoise(self):
		self.addNoise = False
		# Fill the image with a green colour (specified as R,G,B)
		self.image.fill((self.red, self.green, self.blue))
	
	def update(self):
	
		#Check if the ball should change colour
		if self.changeBcolour:
			if self.intensity == "bright":
				self.changeBallColourLight()
				self.image.fill((self.red, self.green, self.blue))
			elif self.intensity == "dark":
				self.changeBallColourDark()
				self.image.fill((self.red, self.green, self.blue))
		
		# Check if the ball has hit a wall
		if self.rect.midtop[1] <= 0:
			# Hit top side
			self.reflectVectorTopBottom()
		elif self.rect.midleft[0] <= 0:
			# Hit left side
			self.reflectVector()
		elif self.rect.midright[0] >= self.displaySize[0]:
			# Hit right side
			self.reflectVector()
		elif self.rect.midbottom[1] >= self.displaySize[1]:
			# Hit bottom side
			self.reflectVectorTopBottom()
		
		# Rotate the ball
		if self.startBRotation:
			self.rotateBall()
		
		#Check if noise should be added to the ball
		if self.addNoise:
			# Fill the 3D array with random values 0-255
			if self.count == 1:
				print 1
				surfarray.blit_array(self.image, self.rgbarray)
			elif self.count == 2:
				print 2
				surfarray.blit_array(self.image, self.rgbarray2)
			elif self.count == 3:
				print 3
				surfarray.blit_array(self.image, self.rgbarray3)
			
			if self.count > 2:
				self.count = 1
			else:
				self.count = self.count + 1
				
		# Move in the direction of the vector
		self.rect.centerx += (self.vector[0] * self.speed)
		self.rect.centery += (self.vector[1] * self.speed)
		
	def reflectVectorTopBottom(self):
	
		#Gets the current angle of the ball and reflects it, for bouncing 
		#off the top and bottom wall
		deltaX = self.vector[0]
		deltaY = - self.vector[1]
		self.vector = (deltaX, deltaY)		
	
	def reflectVector(self):
		
		# Gets the current angle of the ball and reflects it, for bouncing
		# off walls
		deltaX = - self.vector[0]
		deltaY = - self.vector[1]
		self.vector = (deltaX, deltaY)
		
		
	def batCollisionTest(self, bat):
	
		# Check if the ball has had a collision with the bat
		if Rect.colliderect(bat.rect, self.rect):
			
			# Work out the difference between the start and end points
			deltaX = self.rect.centerx - bat.rect.centerx
			deltaY = self.rect.centery - bat.rect.centery
			
			# Make the values smaller so it's not too fast
			deltaX = deltaX / 12
			deltaY = deltaY / 12
			
			# Set the balls new direction
			self.vector = (deltaX, deltaY)
			
if __name__ == '__main__':
	game = PiPong()
	game.run()
