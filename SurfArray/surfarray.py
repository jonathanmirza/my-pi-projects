#!/usr/bin/env python

try:
    import numpy as N
    import pygame.surfarray as surfarray
except ImportError:
    raise ImportError, "Numeric and Surfarray are required."
    
allBlack = N.zeros((128, 128))
surfdemo_show(allBlack, 'allBlack')
