#!/usr/python

# Author: Jonny Mirza
# Date: 27th April 2015
# Copyright 2015

# Use this script to automatically sort scanned PDF files.
# The script will search through PDF documents looking for specified keywords using the
# OCR algorithm.
# This script has been designed to run on the Raspberry Pi 2 B+

# Algorithm:
#       Load PDF
#       Scan the first 50% of the document and store as string
#       Search through the string and look for specified keywords
#       Count how many times keywords appear in the document
#       Calculate the probability of a match
#       Confirm the recipient
#       Establist the date of the document
#       Establish the type of the document i.e.  bank statement, utility bill
#       Rename the document using the following name format SENDER-DOCTYPE-DOCDATE
#       Create folder for the document if it doesn't exist
#       Copy the document to the new/existing folder
#       Delete the original

import os
import sys
from PIL import Image

path = "home/pi/images/scans/"

def findKeyWords():
    
    # Read the keywords file
    f = open("keywords.txt", "r")
    keywords = f.readlines()
    keywordCounter = {}
    
    # get list of filenames
    files = os.listdir(path)
    for file in files:
        image = Image.open(file)
        imageTxt = image_to_string(image)
        for keyword in keywords:
            for docWord in imageTxt:
                if keyword == docWord:
                    if keword in keywordCounter:
                        keywordCounter[keyword] += 1
                    else:
                        keywordCounter[keyword] = 1
    
                        