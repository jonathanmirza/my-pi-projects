#!/usr/bin/env python3

import sys
import os
import csv

jobRolesList = []

with open('C:\\Users\\Jonny\\Dropbox\\PRDb\\CSV Files\\Tables Data\\OutletSubjectsIDs.csv', newline='') as jobRolesCsv:
	jobRolesLineReader = csv.reader(jobRolesCsv, dialect='excel', delimiter=',')
	for row in jobRolesLineReader:
		jobRolesList.append(row)

with open('c:\\users\\Jonny\\Dropbox\\PRDb\\CSV Files\\OutletSubjects.csv', newline='') as csvfile:
	with open('OutletSubjects_Final.csv', "w") as ofile:
		lineReader = csv.reader(csvfile, dialect='excel', delimiter=',')
		writer = csv.writer(ofile, dialect='excel', delimiter=',')
		for row in lineReader:
			row = set(row)
			row = filter(None, row)
			row = list(row)
			for words in row:
				for roles in jobRolesList:
					if words == roles[1]:
						wordsElementID = row.index(words)
						jobElementID = jobRolesList.index(roles)
						row[wordsElementID] = roles[0]
			print(row)
			writer.writerow(row)
