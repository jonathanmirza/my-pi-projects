#!/usr/python
# Author: Jonny Mirza
# Date: April 2015

# Standard UK photo print size 6x4 which is equal to 432x288px
# Three photos are captured using the picamera library
# The pictures are resized to three different sizes: Photo 1: 216x288 (0,0), Photo 2: 215x144 (217,0), Photo 3: 216,143 (217,145)
# Save the images as PIL objects
# Create a blank PIL image and copy the three captured photos into the new PIL image
# Ensure that the photos are placed at the co-ordinates specified above


import time
import picamera
import io
import StringIO
import cups
import RPi.GPIO as GPIO
import pygame
import os
from pygame.locals import *
from PIL import Image
from time import sleep

camera = picamera.PiCamera()

red_button = 13
green_button = 11

GPIO.setmode(GPIO.BOARD)
GPIO.setup(red_button, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(green_button, GPIO.IN, GPIO.PUD_UP)

happyWithPhoto = False

printSuccess = {'IPP_JOB_COMPLETED' : 9}

class cPhoto:
		
	def __init__(self):
		hello = 1
		pygame.init()
		self.screenSize = max(pygame.display.list_modes())
		self.screen = pygame.display.set_mode(self.screenSize, pygame.FULLSCREEN)
		self.background = pygame.Surface(self.screen.get_size())
		self.background = self.background.convert()
		self.background.fill((0,0,0))
		self.numberFont = pygame.font.Font(None, 1024)
		self.smileFont = pygame.font.Font(None, 800)
		self.text = self.numberFont.render("3", 1, (255,0,0))
		self.smileText = self.smileFont.render("3", 1, (255,0,0))
		self.textpos = self.text.get_rect()
		self.textpos.centerx = self.background.get_rect().centerx
			
	def takePhoto(self, picNumber):
		print("takePhoto function called")
		print(picNumber)
		stream = io.BytesIO()
		if picNumber == 1:
			camera.resolution = (960, 1080)
			time.sleep(1)
			self.pyGameCountDown()
			camera.capture(stream, format='png')
			print("takePhoto:camera.capture complete")
		if picNumber == 2 or picNumber == 3:
			camera.resolution = (1920, 1080)
			time.sleep(1)
			self.pyGameCountDown()
			camera.capture(stream, format='png', resize=(960, 540))
			print("takePhoto:camera.capture complete for pic 2 | 3")
		stream.seek(0)
		print("takePhoto:stream.seek complete")
		image = Image.open(stream)
		print("takePhoto:Image.open(stream) complete")
		print("takePhoto complete")
		return image
	
	def pyGameCountDown(self):
		self.textpos.centerx = self.background.get_rect().centerx
		self.numberText = self.numberFont.render("3", 1, (255,0,0))
		self.background.blit(self.numberText, self.textpos)
		self.screen.blit(self.background, (0,0))
		pygame.display.flip()
		time.sleep(1.5)
		self.background.fill((0,0,0))
		self.screen.blit(self.background, (0,0))
		self.numberText = self.numberFont.render("2", 1, (255,0,0))
		self.background.blit(self.numberText, self.textpos)
		self.screen.blit(self.background, (0,0))
		pygame.display.flip()
		time.sleep(1.5)
		self.background.fill((0,0,0))
		self.screen.blit(self.background, (0,0))
		self.numberText = self.numberFont.render("1", 1, (255,0,0))
		self.background.blit(self.numberText, self.textpos)
		self.screen.blit(self.background, (0,0))
		pygame.display.flip()
		time.sleep(1.5)
		self.background.fill((0,0,0))
		self.screen.blit(self.background, (0,0))
		self.textpos.midleft = self.background.get_rect().midleft
		self.smileText = self.smileFont.render("Smile!", 1, (255,0,0))
		self.background.blit(self.smileText, self.textpos)
		self.screen.blit(self.background, (0,0))
		pygame.display.flip()
		time.sleep(1)
		self.background.fill((0,0,0))
		self.screen.blit(self.background, (0,0))
		pygame.display.flip()
	
	def createCollage(self, image1, image2, image3):
		print("createCollage called")
		collage = Image.new("RGB", (1920, 1080))
		print("createCollage:Image.new complete")
		collage.paste(image1, (0,0))
		print("createCollage:collage.paste(img1) complete")
		collage.paste(image2, (961,0))
		print("createCollage:collage.paste(img2) complete")
		collage.paste(image3, (961, 541))
		print("createCollage:collage.paste(img3) complete")
		print("createCollate complete")
		return collage
	
	def savePhoto(self, photo):
		timeString = time.strftime("%Y%m%d-%H%M%S.png")
		photo.save("photo" + timeString)
		print("savePhoto complete")
		return timeString
	
	def printPhoto(self, timeString):
		# Pring the file using CUPS
		conn = cups.Connection()
		# Create the file name string
		fileName = "photo" + str(timeString)
		print(fileName)
		print("printPhoto:cups.Connection established")
		printers = conn.getPrinters()
		print("printPhoto:conn.getPrinters")
		for printer in printers:
			print(printer,printers[printer]["device-uri"])
		printer_name = printers.keys()[0]
		job = conn.printFile(printer_name, fileName, "Python_Status_print", {})
		print("printPhoto:conn.printFile - printing " + fileName)
		jobState = ["job-state"]
		while True:
			printStatus = conn.getJobAttributes(job, jobState)
			printStatus = printStatus['job-state']
			if printStatus == 9:
				print("Printing complate, exiting status check loop")
				break
		print("Print Successfull")
		camera.annotate_text = 'Printing complete!'
		time.sleep(2)
		camera.annotate_text = ''
		
	def displayCollage(self, collage):
		print("Photo:displayCollage called")
		timeString = photo.savePhoto(collage)
		tmpPreviewPhoto = "photo" + str(timeString)
		print("Photo:displayCollage - generating filename string")
		pygamePrevImg = pygame.image.load(tmpPreviewPhoto)
		print("Photo:displayCollage - pygame img loaded into memory")
		self.screen.blit(pygamePrevImg, (0,0))
		pygame.display.flip()
		print("Photo:displayCollage - show the collage for 5 seconds")
		time.sleep(10)
		self.background.fill((0,0,0))
		self.screen.blit(self.background, (0,0))
		pygame.display.flip()
		return timeString

# Create instance of cPhoto class				
photo = cPhoto()
print("photo class created")

camera.start_preview()
camera.preview.alpha = 200

# Main program loop
while True:
	
	# Loop until the red button has been pressed
	print("Entering the press green button to begin loop")
	while True:
		camera.annotate_text = 'Press the green button to begin!'
		if GPIO.input(green_button) == False:
			print("Red button pressed")
			camera.annotate_text = ''
			print("Setting happyWithPhoto = False")
			happyWithPhoto = False
			break
	
	# Loop until the user is happy with the picture
	while happyWithPhoto == False:
		img1 = photo.takePhoto(1)
		print("img1 captured")
		img2 = photo.takePhoto(2)
		print("img2 captured")
		img3 = photo.takePhoto(3)
		print("img3 captured")

		print("Creating collage")
		collage = photo.createCollage(img1, img2, img3)
		print("Collage complete")

		print("Stopping preview")
		camera.stop_preview()
		print("Preview stopped")
		
		print("Displaying pygame collage")
		timeString = photo.displayCollage(collage)
		
		camera.start_preview()
		print("Restarting live preview")
		camera.annotate_text = 'Press the red button to take again or the green button to print'
		
		print("Entering user accept/reject photo loop")
		while True:
			if GPIO.input(red_button) == False:
				print("Red button pressed, breaking out of loop")
				happyWithPhoto = False
				print("Photo rejected, deleting photo")
				tmpFileName = "photo" + str(timeString)
				os.remove(tmpFileName)
				print(str(tmpFileName) + " deleted from disk.")
				camera.annotate_text = ''
				camera.preview.alpha = 200
				break
			if GPIO.input(green_button) == False:
				print("Green button pressed, breaking out of loop")
				happyWithPhoto = True
				camera.annotate_text = 'Printing...'
				camera.preview.alpha = 200
				break
			
	print("photo:printPhoto called")
	photo.printPhoto(timeString)
