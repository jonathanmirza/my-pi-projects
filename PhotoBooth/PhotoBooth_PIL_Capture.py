#!/usr/python

import io
import time
import picamera
from PIL import Image

# Create the in-memory stream
stream = io.BytesIO()
print("io.BytesIO create")
with picamera.PiCamera() as camera:
    camera.start_preview()
    time.sleep(2)
    camera.capture(stream, format='jpeg')
    print("picture taken")
# "Rewind" the stream to the beginning so we can read its content
stream.seek(0)
print("stream.seek complete")
image = Image.open(stream)
print("PIL object created")
camera.stop_preview()